addEventListener('load', inicialize, false);

var results=0;
var selInputs = null;

function inicialize(){
	selInputs = document.querySelectorAll('input');
	for (var i=0; i<=selInputs.length-1; i++){
		selInputs[i].addEventListener('change', onClick);
		if(selInputs[i].checked){
			results++;
		}
	}
	output(results);
}

function onClick(e){
	var fieldClass = document.querySelector('.list-block');
	results += (e.target.checked)? 1 : -1;
	if (results === selInputs.length){
		fieldClass.classList.toggle('complete');
	} else if(results < selInputs.length && fieldClass.classList.contains('complete')){
		fieldClass.classList.toggle('complete');
	}
	output(results);
}

function output(result){
	var output = document.querySelector('output');
	output.innerHTML = results + ' из ' + selInputs.length;
}