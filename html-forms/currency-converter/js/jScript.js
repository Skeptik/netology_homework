addEventListener('load', loading, false);

const loader = document.querySelector('#loader');
const xhr = new XMLHttpRequest();
const selectFrom = document.querySelector('#from');
const selectTo = document.querySelector('#to');
const source = document.querySelector('#source');

function loading (){
	var loader = document.querySelector('#loader');
	if (loader.classList.contains('hidden')){
		loader.classList.remove('hidden');
	}
	selectFrom.addEventListener('input', converter);
	selectTo.addEventListener('input', converter);
	source.addEventListener('input', converter);
	xhr.addEventListener('load', onLoad);
	xhr.open('GET', "https://neto-api.herokuapp.com/currency");
	xhr.send();
}

function converter(){
	var result = source.value*selectFrom.value/selectTo.value;
	document.querySelector('#result').innerHTML=result.toFixed(2);
}

function onLoad(){
	var content = document.querySelector('#content');
	loader.classList.add('hidden');
	content.classList.remove('hidden');
	var json = JSON.parse(xhr.response);
	fillingSelect(json);
}

function fillingSelect(json){
	for (var i=0; i<=json.length-1; i++){
		var objOptionFrom = document.createElement('option');
		objOptionFrom.setAttribute('label', json[i].code);
		objOptionFrom.setAttribute('value', json[i].value);
		objOptionTo = objOptionFrom.cloneNode(true);
		selectFrom.appendChild(objOptionFrom);
		selectTo.appendChild(objOptionTo);
	}
}