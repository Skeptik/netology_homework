addEventListener('load', inicialize, false);

var textarea = document.querySelector('textarea');
var main = document.querySelector('main'); // Форма вывода
var form = document.querySelector('form'); // Форма ввода
var button = document.querySelectorAll('.button-contact');
var input = document.querySelectorAll('input');
var arrElementsField  = [];
var check = 0;

function inicialize(){
	var index = document.querySelector('[name="zip"]');
	index.addEventListener('keypress', onKeyPress);
	button[0].addEventListener('click', reverseClass);
	button[1].addEventListener('click', reverseClass);
	addElementsArr();
	onChange();
}

function onKeyPress(event){
	if (!event.keyCode === 13 && event.keyCode < 45 || event.keyCode > 57){
		event.returnValue = false;
	}
}

function onChange(){
	arrElementsField.map((i)=>{
		i.onchange=function(){
			if (i.hasAttribute('data-answer') && i.getAttribute('data-answer') === i.value){
				return false;
			}else if (i.value.length === 0){
				i.removeAttribute('data-answer');
				test(i);
				return false;
			}else if (i.value.length!=0 && i.hasAttribute('data-answer')){
				addAttributeInput(i);
				addAttributeForm(i.getAttribute('name'), i.value);
				return false;
			}
			addAttributeInput(i);
			test(i);
		}
	});
}

function test (i){
	addAttributeForm(i.getAttribute('name'), i.value);
	(i.value.length!=0)?check++: check--;
	(check>10)?button[0].disabled=false:button[0].disabled=true;
}

function addAttributeInput(i){
	i.setAttribute('data-answer', i.value);
}

function addElementsArr(){
	for (var i =0; i<=input.length-1; i++){
		arrElementsField.push(input[i]);
	}
	arrElementsField.push(textarea);
}

function reverseClass(event){
	event.preventDefault();
	main.classList.toggle('hidden');
	form.classList.toggle('hidden');
}

function addAttributeForm(attributeName, value){
	var elem = document.querySelector('output#'+attributeName);
	if(elem){
		elem.innerHTML = value;
	}else{
		return false;
	}
}