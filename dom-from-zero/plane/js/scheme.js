'use strict'

const url = 'https://neto-api.herokuapp.com/plane/';
const btnSeatMap = document.querySelector('#btnSeatMap');
const acSelect = document.querySelector('#acSelect');
const seatMapDiv = document.querySelector('#seatMapDiv');
const btnSetFull = document.querySelector('#btnSetFull');
const btnSetEmpty = document.querySelector('#btnSetEmpty');
const seatMapTitle = document.querySelector('#seatMapTitle');
const totalPax = document.querySelector('#totalPax');
const totalAdult = document.querySelector('#totalAdult');
const totalHalf = document.querySelector('#totalHalf');
addEventListener('load', inicialization, false);
var checkCtrl = false;

function inicialization (){
    btnSeatMap.addEventListener('click', onClickSeatMap);
    btnSetFull.disabled = true;
    btnSetEmpty.disabled = true;
    btnSetFull.addEventListener('click', onBtnSetFull);
    btnSetEmpty.addEventListener('click', onBtnSetEmpty);
    seatMapDiv.addEventListener('click', onCheck);
    document.addEventListener('keydown', keyDown, false);
    document.addEventListener('keyup', keyUp, false);
}

function keyDown(e){
    if(e.key == 'Control'){
        checkCtrl = true;
    }
}

function keyUp(){
    checkCtrl = false;
}

function onCheck(e){
    if(e.target.tagName == 'SPAN'){
        placeClick(e.target.parentNode);
    }
    if(e.target.tagName == 'DIV' && e.target.classList.contains('seat')){
        placeClick(e.target);
    }
}

function placeClick(div){
    if(checkCtrl){
        (div.classList.contains('adult')) ? div.classList.toggle('adult'): '';
        div.classList.toggle('half');
    }else{
        (div.classList.contains('half')) ? div.classList.toggle('half'): '';
        div.classList.toggle('adult');
    }
    checkPlaces();
}

function onClickSeatMap(e){
    e.preventDefault();
    const id = acSelect.options[acSelect.selectedIndex];
    seatMapTitle.textContent = id.text;
    var promise = fetch(url+id.value,{
        credentials: 'omit',
        method: 'GET',
    })
    .then((res)=>{
        if(200<=res.status && res.status<300){
            return res;
        }
        throw new Error(response.statusText);
    })
    .then((res) => res.json())
    .then((data) =>{
        if(data.error){
            console.log(data.message);
            return;
        }else{
            let fragment;
            seatMapDiv.textContent = '';
            for(let i=0; i<=data.scheme.length-1; i++){
                fragment = browser(rowPlane_6(data, data.scheme[i], i));
                seatMapDiv.appendChild(fragment);
            }
            btnSetFull.disabled = false;
            btnSetEmpty.disabled = false;
            checkPlaces();
        }
    })
    .catch((error)=>{
        console.log('Ошибка ' + error);
    });
}

function rowPlane_6(data, row, indx){
    var check_4;
    var check_6;
    if(row == 6){
        check_4 = 'seat';
        check_6 = 'seat';
    }else if(row ==4){
        check_4 = 'no-seat';
        check_6 = 'seat';
    }else if(row == 0){
        check_4 = 'no-seat';
        check_6 = 'no-seat';
    }
    return {
        tag: 'div',
        cls: 'row seating-row text-center',
        content: [
            {
                tag: 'div',
                cls: 'col-xs-1 row-number',
                content: [
                    {
                        tag: 'h2',
                        cls: '',
                        txt: indx+1
                    }
                ],
            },
            {
                tag: 'div',
                cls: 'col-xs-5',
                content: [
                    {
                        tag: 'div',
                        cls: 'col-xs-4 ' + check_4,
                        content: [
                            {
                                tag: 'span',
                                cls: 'seat-label',
                                txt:  (row == 6) ? data.letters6[0] : '',
                            }
                        ]
                    },
                    {
                        tag: 'div',
                        cls: 'col-xs-4 ' + check_6,
                        content: [
                            {
                                tag: 'span',
                                cls: 'seat-label',
                                txt: (row == 6) ? data.letters6[1] : (row == 4) ? data.letters4[0]: '',
                            }
                        ]
                    },
                    {
                        tag: 'div',
                        cls: 'col-xs-4 ' + check_6,
                        content: [
                            {
                                tag: 'span',
                                cls: 'seat-label',
                                txt: (row == 6) ? data.letters6[2] : (row == 4) ? data.letters4[1]: '',
                            }
                        ]
                    }
                ]
            },
            {
                tag: 'div',
                cls: 'col-xs-5',
                content: [
                    {
                        tag: 'div',
                        cls: 'col-xs-4 ' + check_6,
                        content: [
                            {
                                tag: 'span',
                                cls: 'seat-label',
                                txt: (row == 6) ? data.letters6[3] : (row == 4) ? data.letters4[2]: '',
                            }
                        ]
                    },
                    {
                        tag: 'div',
                        cls: 'col-xs-4 ' + check_6,
                        content: [
                            {
                                tag: 'span',
                                cls: 'seat-label',
                                txt: (row == 6) ? data.letters6[4] : (row == 4) ? data.letters4[3]: '',
                            }
                        ]
                    },
                    {
                        tag: 'div',
                        cls: 'col-xs-4 ' + check_4,
                        content: [
                            {
                                tag: 'span',
                                cls: 'seat-label',
                                txt: (row == 6) ? data.letters6[5] : '',
                            }
                        ]
                    }
                ]
            }
        ]
    };
}

function browser(block) {
    if (Array.isArray(block)) {
        return block.reduce(function(f, item) {
            f.appendChild(browser(item));
            return f;
        }, document.createDocumentFragment());
    }

    const element = document.createElement(block.tag);
    element.className = block.cls;
    if (block.content) {
        element.appendChild(browser(block.content));
    }
    if (block.txt){
        element.textContent = block.txt;
    }
    return element;
}


function onBtnSetFull (e){
    e.preventDefault();
    var places = document.querySelectorAll('div > .col-xs-4.seat');
    for (let i=0; i<=places.length-1; i++){
        if(!places[i].classList.contains('adult')){
            places[i].classList.toggle('adult');
        }
        if(places[i].classList.contains('half')){
            places[i].classList.toggle('half');
        }
    }
    checkPlaces();
}

function onBtnSetEmpty(e){
    e.preventDefault();
    var places = document.querySelectorAll('div > .col-xs-4.seat');
    for (let i=0; i<=places.length-1; i++){
        if(places[i].classList.contains('adult')){
            places[i].classList.toggle('adult');
        }
        if(places[i].classList.contains('half')){
            places[i].classList.toggle('half');
        }
    }
    checkPlaces();
}

function checkPlaces(){
    var places = document.querySelectorAll('div > .col-xs-4.seat');
    var adult = 0;
    var half = 0;
    for (let i=0; i<=places.length-1; i++){
        if(places[i].classList.contains('adult')){
            adult++;
        }
        if (places[i].classList.contains('half')){
            half++;
        }
    }
    totalPax.textContent = adult+half;
    totalAdult.textContent = adult;
    totalHalf.textContent = half;
}