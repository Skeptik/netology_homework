'use strict';
function createElement(block){
    if ((typeof block === 'string')) {
            return document.createTextNode(block);
        }
    if (Array.isArray(block)) {
        return block.reduce(function(f, item) {
            f.appendChild(createElement(item));
            return f;
        }, document.createDocumentFragment());
    }
    const element = document.createElement(block.name);
    if (block.childs) {
            element.appendChild(createElement(block.childs));
    }
    if(block.props){
        Object.keys(block.props).forEach(function(key) {
            element.setAttribute(key, block.props[key]);
        });
    }
    return element;
}
