'use strict';

function showComments(list) {
  const commentsContainer = document.querySelector('.comments');
  const comments = list.map(createComment);
  const fragment = comments.reduce((fragment, currentValue)=>{
    fragment.appendChild(currentValue);
    return fragment;
  }, document.createDocumentFragment());
  commentsContainer.appendChild(fragment);
}

function createComment(comment) {
  var objDiv = document.createElement('div');
  objDiv.className = 'comment-wrap';
  var objDiv_2 = document.createElement('div');
  objDiv_2.className = 'photo';
  objDiv_2.setAttribute("title", comment.author.name);
  var objDiv_3 = document.createElement('div');
  objDiv_3.className = 'avatar';
  objDiv_3.style.backgroundImage = "url("+comment.author.pic+")";
  objDiv_2.appendChild(objDiv_3);           
  var objDiv_4 = document.createElement('div');
  objDiv_4.className = 'comment-block';
  var objP = document.createElement('p');
  objP.className = 'comment-text';
  objP.textContent = comment.text;
  var objDiv_5 = document.createElement('div');
  objDiv_5.className = 'bottom-comment';
  var objDiv_6 = document.createElement('div');
  objDiv_6.className = 'comment-date';
  objDiv_6.textContent = new Date(comment.date).toLocaleString('ru-Ru');
  var objUl = document.createElement('ul');
  objUl.className = 'comment-actions';
  var objLi = document.createElement('li');
  objLi.className = 'complain';
  objLi.textContent = 'Пожаловаться';
  var objLi_2 = document.createElement('li');
  objLi_2.className = 'reply';
  objLi_2.textContent = 'Ответить';
  objUl.appendChild(objLi);
  objUl.appendChild(objLi_2);
  objDiv_5.appendChild(objDiv_6);
  objDiv_5.appendChild(objUl);
  objDiv_4.appendChild(objP);
  objDiv_4.appendChild(objDiv_5);
  objDiv.appendChild(objDiv_2);
  objDiv.appendChild(objDiv_4);
  return objDiv;

  // return `<div class="comment-wrap">
  //   <div class="photo" title="${comment.author.name}">
  //     <div class="avatar" style="background-image: url('${comment.author.pic}')"></div>
  //   </div>
  //   <div class="comment-block">
  //     <p class="comment-text">
  //       ${comment.text.split('\n').join('<br>')}
  //     </p>
  //     <div class="bottom-comment">
  //       <div class="comment-date">${new Date(comment.date).toLocaleString('ru-Ru')}</div>
  //       <ul class="comment-actions">
  //         <li class="complain">Пожаловаться</li>
  //         <li class="reply">Ответить</li>
  //       </ul>
  //     </div>
  //   </div>
  // </div>`
}

fetch('https://neto-api.herokuapp.com/comments')
  .then(res => res.json())
  .then(showComments);