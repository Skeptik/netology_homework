'use strict';
const ulrDataRecipe = 'https://neto-api.herokuapp.com/food/42';
const urlRatingRecipe = 'https://neto-api.herokuapp.com/food/42/rating';
const urlUsers = 'https://neto-api.herokuapp.com/food/42/consumers';
const dataPic = document.querySelector('[data-pic]');
const dataTitle = document.querySelector('[data-title]');
const dataIngredients = document.querySelector('[data-ingredients]');
const dataRating = document.querySelector('[data-rating]');
const dataStar = document.querySelector('[data-star]');
const dataVotes = document.querySelector('[data-votes]');
const dataConsumers = document.querySelector('[data-consumers]');

function dataRecipe(data){
		dataPic.style.backgroundImage = 'url('+data.pic+')';
		dataTitle.textContent = data.title;
		var arrIngredients = [];
		for (let i=0; i<=data.ingredients.length-1; i++){
			arrIngredients.push(data.ingredients[i]);
		}
		var ingr = arrIngredients.join(', ');
		dataIngredients.textContent = ingr;
}

function ratingRecipr(data){
	dataRating.textContent = data.rating.toFixed(2);
	dataStar.style.width = data.rating * 100 / 10 +'%';
	dataVotes.textContent = '(' + data.votes + ' оценок)';
}

function users (data){
	for(let i=0; i<=data.consumers.length-1; i++){
		var imgObj = document.createElement('img');
		imgObj.src = data.consumers[i].pic;
		imgObj.title = data.consumers[i].name;
		dataConsumers.appendChild(imgObj);
	}
	var spanObj = document.createElement('span');
	spanObj.textContent = '(+' + data.total + ')';
	dataConsumers.appendChild(spanObj);
}

function loadData (url, func){
	const script = document.createElement('script');
	script.src = url + '?jsonp=' + func;
	document.body.appendChild(script);
	document.body.removeChild(script);
}

loadData(ulrDataRecipe, 'dataRecipe');
loadData(urlRatingRecipe, 'ratingRecipr');
loadData(urlUsers, 'users');
