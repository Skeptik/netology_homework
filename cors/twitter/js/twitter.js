'use strict';
const urlp = 'https://neto-api.herokuapp.com/twitter/jsonp';
const dataWallpaper = document.querySelector('[data-wallpaper]');
const dataUsername = document.querySelector('[data-username]');
const dataDescription = document.querySelector('[data-description]');
const dataPic = document.querySelector('[data-pic]');
const dataTweets = document.querySelector('[data-tweets]');
const dataFollowers = document.querySelector('[data-followers]');
const dataFollowing = document.querySelector('[data-following]');

function callback(data){
	dataWallpaper.src = data.wallpaper;
	dataUsername.textContent = data.username;
	dataDescription.textContent = data.description;
	dataPic.src = data.pic;
	dataTweets.textContent = data.tweets;
	dataFollowers.textContent = data.followers;
	dataFollowing.textContent = data.following;
}


function loadData(url){
	const script = document.createElement('script');
	script.src = urlp;
	document.body.appendChild(script);
}

loadData(urlp);