'use strict';

const urlProfile = 'https://neto-api.herokuapp.com/profile/me';
const urlTehnology = 'https://neto-api.herokuapp.com/profile/';
const dataName = document.querySelector('[data-name]');
const dataDescription = document.querySelector('[data-description]');
const dataPic = document.querySelector('[data-pic]');
const dataPosition = document.querySelector('[data-position]');
const dataTechnologies = document.querySelector('[data-technologies]');
const content = document.querySelector('.content');
var url;

function callback(data){
	if(data.id){
		url = urlTehnology+data.id+'/technologies';
		dataName.textContent = data.name;
		dataDescription.textContent = data.description;
		dataPic.src = data.pic;
		dataPosition.textContent = data.position;
		loadData(url, 'tehnology');
	}
}

function tehnology (data){
	for (let i=0; i<=data.length-1; i++){
		var span = document.createElement('span');
		span.setAttribute('class', 'devicons devicons-' + data[i]);
		dataTechnologies.appendChild(span);
	}
	content.style.display = 'initial';
}


function loadData (url, func){
		const script = document.createElement('script');
		script.src = url + '?jsonp=' + func;
		document.body.appendChild(script);
		document.body.removeChild(script);
}

loadData(urlProfile, 'callback');