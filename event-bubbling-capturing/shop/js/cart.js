'use strict';

addEventListener('load', inicialize, false);

function inicialize(){
	const main = document.querySelector('.items-list');
	main.addEventListener('click', onClick, true);
}

function onClick (e){
	if(!e.target.classList.contains('add-to-cart')){
		return false;
	}
	var title = e.target.getAttribute('data-title');
	var price = e.target.getAttribute('data-price');
	addToCart(new Product(title, price));
}

function Product(title, price){
	this.title = title;
	this.price = price;
}