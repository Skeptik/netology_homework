'use strict';
var check = null;

function handleTableClick(event) {
	var name = event.target.getAttribute('data-prop-name');
	if(event.target.hasAttribute('data-dir')){
		check = event.target.getAttribute('data-dir');
		event.target.setAttribute('data-dir', check *= -1);
	}else{
		check = 1;
		event.target.setAttribute('data-dir', check);
	}
	event.currentTarget.setAttribute('data-sort-by', name);
	sortTable(name, check);
}
