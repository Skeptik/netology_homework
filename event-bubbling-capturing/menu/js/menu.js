'use strict';

function toggleMenu(event) {
  event.preventDefault();
  if (this.classList.contains('show') && !event.target.hasAttribute('href')) {
    this.classList.remove('show');
    this.classList.add('hide');
  } else {
    this.classList.add('show');
    this.classList.remove('hide');
  }
}

function openLink(event) {
  console.log(this.textContent);
}

function init(node) {
  node.addEventListener('click', toggleMenu, true);
}

function initLink(node) {
  if (node.dataset.toggle) {
    return;
  }
  node.addEventListener('click', openLink);
}

Array
  .from(document.querySelectorAll('.dropdown'))
  .forEach(init);

Array
  .from(document.querySelectorAll('a'))
  .forEach(initLink);