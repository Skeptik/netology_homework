addEventListener('load', inicialize, false);

const done = document.querySelector('.done');
const undone = document.querySelector('.undone');

function inicialize(){
	const label = document.querySelectorAll('input');
	for (var i = 0; i <= label.length-1 ; i++){
		label[i].addEventListener('click', onClick, false);
	}
}

function onClick(e){
	var parent = e.target.parentNode;
	if (parent.parentNode.getAttribute('class') == 'done'){
		undone.appendChild(parent);
	}else{
		done.appendChild(parent);
	}
}
