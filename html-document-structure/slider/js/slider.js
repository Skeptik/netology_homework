addEventListener('load', inicialize, false);

const prevButton = document.querySelector('[data-action=prev]');
const nextButton = document.querySelector('[data-action=next]');
const firstButton = document.querySelector('[data-action=first]');
const lastButton = document.querySelector('[data-action=last]');
const slides = document.querySelector('.slides');
var activeSlide;

function inicialize (){
	document.querySelector('li').classList.toggle('slide-current');
	prevButton.addEventListener('click', () => onClick ('prev'));
	nextButton.addEventListener('click', () => onClick ('next'));
	firstButton.addEventListener('click', () => onClick ('first'));
	lastButton.addEventListener('click', () => onClick ('last'));
	activeSlide = document.querySelector('.slide-current');
	activeButton();
}

function onClick(direction){
	if(event.target.classList.contains('disabled')){
		return false;
	}
	activeSlide.classList.toggle('slide-current');
	var nextButton;
	switch (direction) {
		case 'prev':
		nextButton = activeSlide.previousElementSibling;
		break;
		case 'next':
		nextButton = activeSlide.nextElementSibling;
		break;
		case 'first':
		nextButton = slides.firstElementChild;
		break;
		case 'last':
		nextButton = slides.lastElementChild;
		break;
	}
	nextButton.classList.toggle('slide-current');
	activeSlide = nextButton;
	activeButton();
}

function activeButton(){
	prevButton.classList.toggle('disabled', activeSlide.previousElementSibling == null);
	nextButton.classList.toggle('disabled', activeSlide.nextElementSibling == null);
	firstButton.classList.toggle('disabled', activeSlide.previousElementSibling == null);
	lastButton.classList.toggle('disabled', activeSlide.nextElementSibling == null);
}