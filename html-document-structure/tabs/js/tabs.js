addEventListener('load', inicialize, false);

const tab = document.querySelector('.tabs-nav');
const tabsContent = document.querySelector('.tabs-content');
var activeArticle, nextSlide;

function inicialize (){
	for (let i=0; i<tabsContent.children.length; i++){
		tabsContent.children[i].classList.toggle('hidden');
		var childTab = tab.firstElementChild.cloneNode(true);
		var name = tabsContent.children[i].getAttribute('data-tab-title');
		var icon = tabsContent.children[i].getAttribute('data-tab-icon');
		childTab.firstElementChild.classList.add(icon);
		childTab.firstElementChild.textContent=name;
		childTab.firstElementChild.addEventListener('click', onClick, false);
		tab.appendChild(childTab);
	}
	tab.removeChild(tab.firstElementChild);
	activeArticle = document.querySelector('[data-tab-title=Шопинг]');
	activeArticle.classList.toggle('hidden');
	nextSlide = tab.firstElementChild.firstElementChild;
	nextSlide.classList.toggle('ui-tabs-active');
}

function onClick (e){
	if(e.target.classList.contains('ui-tabs-active')){
		return false;
	}
	var name = e.target.textContent;
	nextSlide.classList.toggle('ui-tabs-active');
	activeArticle.classList.toggle('hidden');
	e.target.classList.toggle('ui-tabs-active');
	nextSlide=e.target;
	switch (name){
		case 'Шопинг':
		activeArticle = document.querySelector('[data-tab-title=Шопинг]'); 
		activeArticle.classList.toggle('hidden');
		break;
		case 'Еда':
		activeArticle = document.querySelector('[data-tab-title=Еда]'); 
		activeArticle.classList.toggle('hidden');
		break;
		case 'Клубы':
		activeArticle = document.querySelector('[data-tab-title=Клубы]');
		activeArticle.classList.toggle('hidden');
		break;
	}
}