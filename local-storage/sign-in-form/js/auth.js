'use strict';

addEventListener('load', inicialize, false);

const signInError = document.querySelector('.sign-in-htm > output');
const signUpError = document.querySelector('.sign-up-htm > output');
var url, outputText, status;

function inicialize (){
	const button = document.querySelectorAll('.button');
	for (let i=0; i<=button.length-1; i++){
		button[i].addEventListener('click', onClick, false);
	}
}

function onClick(e){
	e.preventDefault();
	const parentClass = e.target.parentNode.parentNode.getAttribute('class');
	const form = document.querySelector('.'+parentClass);
	const formData = new FormData(form);
	var results = new Object();
	for (const [k,v] of formData){
		results[k] = v;
	} 
	const json = JSON.stringify(results);
	if (e.target.value === 'Войти'){
		url = 'https://neto-api.herokuapp.com/signin';
		outputText = signInError;
		status = 'авторизован.';
	}else if (e.target.value === 'Зарегистрироваться'){
		url = 'https://neto-api.herokuapp.com/signup';
		outputText = signUpError;
		status = 'зарегистрирован.';
	}
	var promise = fetch(url,{
		body : json,
		credentials: 'omit',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then((res)=>{
		if(200<=res.status && res.status<300){
			return res;
		}
		throw new Error(response.statusText);
	})
	.then((res) => res.json())
	.then((data) =>{
		if(data.error){
			outputText.textContent = data.message;
			return;
		}else{
			outputText.textContent = 'Пользователь ' + data.name + ' успешно ' + status;
		}
	})
	.catch((error)=>{
		console.log('Ошибка ' + error);
	});
}