'use strict';
addEventListener('load', inicialize, false);

const counter = document.querySelector('#counter');

function inicialize (){
	if(!localStorage.check){
		localStorage.check = 0;
	}
	counter.textContent = localStorage.check;
	const button = document.querySelectorAll('button');
	for (let i=0; i<=button.length-1; i++){
		button[i].addEventListener('click', onClick);
	}
}

function onClick (e){
	if(!isNaN(localStorage.check)){
		switch (e.target.id){
			case 'decrement':
			localStorage.check--;
			break;
			case 'increment':
			localStorage.check++;
			break;
			case 'reset':
			localStorage.check=0;
			break;
		}
	counter.textContent = localStorage.check;
	}else{
		counter.textContent = 'Счетчик не корректный';
	}
}
