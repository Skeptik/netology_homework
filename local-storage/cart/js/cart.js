'use strict';

const urlColors = 'https://neto-api.herokuapp.com/cart/colors';
const urlSize = 'https://neto-api.herokuapp.com/cart/sizes';
const urlAddToCart = 'https://neto-api.herokuapp.com/cart';
const urlRemove = 'https://neto-api.herokuapp.com/cart/remove';
const colorSwatch = document.querySelector('#colorSwatch');
const sizeSwatch = document.querySelector('#sizeSwatch');
const divSwatch = document.querySelector('.swatches');
const button = document.querySelector('#AddToCart');
const form = document.querySelector('#AddToCartForm');
const bask = document.querySelector('#quick-cart');
const id = form.getAttribute('data-product-id');
var activeColor, activeSize;

addEventListener('load', inicialize, false);

function inicialize (){
	divSwatch.addEventListener('click', onClick, false);
	button.addEventListener('click', addToCart, false);
	Promise.all([fetch(urlColors), fetch(urlSize)])
		.then(([res1, res2]) =>{
			res1.json()
				.then((data)=>{
					color(data);
				});
			res2.json()
				.then((data)=>{
					size(data);
				});
		})
		.catch((error) => {
			console.log('Ошибка ' + error);
		});
}

function color(data){
	for(let i=0; i<=data.length-1; i++){
		var divObj = document.createElement('div');
		divObj.setAttribute('data-value', data[i].type);
		divObj.setAttribute('class', 'swatch-element '+'color ' + data[i].type);
		var avail = (data[i].isAvailable)?true:false;
		var divObj_2 = document.createElement('div');
		divObj_2.setAttribute('class', 'tooltip');
		divObj_2.textContent = data[i].title;
		var inputObj = document.createElement('input');
		inputObj.setAttribute('quickbeam', 'color');
		inputObj.id = 'swatch-1-' + data[i].type;
		inputObj.type = 'radio';
		inputObj.name = 'color';
		inputObj.value = data[i].type;
		if(avail){
			divObj.classList.add('available');
			inputObj.disabled = false;
		}else{
			divObj.classList.add('soldout');
			inputObj.disabled = true;
		}
		var labelObj = document.createElement('label');
		labelObj.setAttribute('for', 'swatch-1-' + data[i].type);
		labelObj.style.borderColor = data[i].type;
		var spanObj = document.createElement('span');
		spanObj.style.backgroundColor = data[i].code;
		var imgObj = document.createElement('img');
		imgObj.setAttribute('class', 'crossed-out');
		imgObj.src = 'https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886';
		labelObj.appendChild(spanObj);
		labelObj.appendChild(imgObj);
		divObj.appendChild(divObj_2);
		divObj.appendChild(inputObj);
		divObj.appendChild(labelObj);
		colorSwatch.appendChild(divObj);
	}
}

function size (data){
	for(let i=0; i<=data.length-1; i++){
		var divObj = document.createElement('div');
		divObj.setAttribute('data-value', data[i].type);
		divObj.setAttribute('class', 'swatch-element '+'plain ' + data[i].type);
		var avail = (data[i].isAvailable)?true:false;
		var inputObj = document.createElement('input');
		inputObj.id = 'swatch-1-' + data[i].type;
		inputObj.type = 'radio';
		inputObj.name = 'size';
		inputObj.value = data[i].type;
		if(avail){
			divObj.classList.add('available');
			inputObj.disabled = false;
		}else{
			divObj.classList.add('soldout');
			inputObj.disabled = true;
		}
		var labelObj = document.createElement('label');
		labelObj.setAttribute('for', 'swatch-1-' + data[i].type);
		labelObj.textContent = data[i].title;
		var imgObj = document.createElement('img');
		imgObj.setAttribute('class', 'crossed-out');
		imgObj.src = 'https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886';
		labelObj.appendChild(imgObj);
		divObj.appendChild(inputObj);
		divObj.appendChild(labelObj);
		sizeSwatch.appendChild(divObj);
	}
	byDefault();
}

function onClick (e){
	if(e.target.tagName == 'SPAN'){
		var colorInput = e.target.parentNode.parentNode.querySelector('input');
		localStorage.colorId = colorInput.id;
		if(activeColor){
			activeColor.removeAttribute('checked');
		}
		activeColor = colorInput;
		activeColor.setAttribute('checked', '');
	}else if (e.target.tagName == 'LABEL'){
		var sizeInput = e.target.parentNode.querySelector('input');
		localStorage.sizeId = sizeInput.id;
		if(sizeInput){
			activeSize.removeAttribute('checked');
		}
		activeSize = sizeInput;
		activeSize.setAttribute('checked', '');
	}
}

function byDefault(){
	if(!localStorage.colorId == 0){
		var cc = document.querySelector('#'+localStorage.colorId);
		cc.setAttribute('checked', '');
		activeColor = cc;
	}
	if(!localStorage.sizeId == 0){
		var ss = document.querySelector('#'+localStorage.sizeId);
		ss.setAttribute('checked', '');
		activeSize = ss;
	}
}

function addToCart(e){
	e.preventDefault();
	var formData = new FormData(form);
	formData.append('productId', id);
	request(urlAddToCart, formData);
}

function basket(data){
	bask.textContent='';
	var divObj = document.createElement('div');
	divObj.setAttribute('class', 'quick-cart-product '+'quick-cart-product-static');
	divObj.id = 'quick-cart-product-' + data[0].id;
	divObj.style.opacity = 1;
	var divObj_2 = document.createElement('div');
	divObj_2.setAttribute('class', 'quick-cart-product-wrap');
	var imgObj = document.createElement('img');
	imgObj.src = data[0].pic;
	imgObj.title = data[0].title;
	var spanObj = document.createElement('span');
	spanObj.setAttribute('class', 's1');
	spanObj.style.backgroundColor = '#000';
	spanObj.style.opacity = .5;
	spanObj.textContent = '$800.00';
	var spanObj_2 = document.createElement('span');
	spanObj_2.setAttribute('class', 's2');
	divObj_2.append(imgObj);
	divObj_2.append(spanObj);
	divObj_2.append(spanObj_2);
	var spanObj_3 = document.createElement('span');
	spanObj_3.setAttribute('class', 'count ' + 'hide ' + 'fadeUp');
	spanObj_3.id = 'quick-cart-product-count-' + data[0].id;
	spanObj_3.textContent = data[0].quantity;
	var spanObj_4 = document.createElement('span');
	spanObj_4.setAttribute('class', 'quick-cart-product-remove '+'remove');
	spanObj_4.setAttribute('data-id', data[0].id);
	spanObj_4.addEventListener('click', remove);
	divObj.append(divObj_2);
	divObj.append(spanObj_3);
	divObj.append(spanObj_4);
	bask.append(divObj);
	sneepBasket(data[0].quantity, data[0].price);
}

function sneepBasket(quantity, price){
	var aObj = document.createElement('a');
	aObj.id = 'quick-cart-pay';
	aObj.setAttribute('quickbeam', 'cart-pay');
	if (quantity == 0){
		aObj.setAttribute('class', 'cart-ico');
	}else{
		aObj.setAttribute('class', 'cart-ico ' + 'open');
	}
	var spanObj = document.createElement('span');
	var strongObj = document.createElement('strong');
	strongObj.setAttribute('class', 'quick-cart-text');
	strongObj.textContent = 'Оформить заказ';
	var br = document.createElement('br');
	strongObj.appendChild(br);
	var spanObj_2 = document.createElement('span');
	spanObj_2.id = 'quick-cart-price';
	spanObj_2.textContent = '$'+quantity*price;
	spanObj.appendChild(strongObj);
	spanObj.appendChild(spanObj_2);
	aObj.appendChild(spanObj);
	bask.appendChild(aObj);
}

function remove(){
	var formData = new FormData(form);
	formData.append('productId', id);
	request(urlRemove, formData);
}

function request (url, formData){
	console.log(formData);
	var promise = fetch(url,{
		body : formData,
		credentials: 'omit',
		method: 'POST',
	})
	.then((res)=>{
		if(200<=res.status && res.status<300){
			return res;
		}
		throw new Error(response.statusText);
	})
	.then((res) => res.json())
	.then((data) =>{
		basket(data);
	})
	.catch((error)=>{
		console.log('Ошибка ' + error);
	});
}