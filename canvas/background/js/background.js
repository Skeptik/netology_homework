addEventListener('load', inicialization, false);

var canvas = document.querySelector('canvas');
var ctx = canvas.getContext("2d");
var rand, bodyWidth, bodyHeight;
var fps = 20;
var arrCircle = [];
var arrCross = [];

function inicialization(){
	rand = Number(selfRandom(50, 200).toFixed());
	bodyWidth = window.innerWidth;
	bodyHeight = window.innerHeight;
	canvas.width = bodyWidth;
	canvas.height = bodyHeight;
	createCircle();
	createCross();
	setInterval(animate, 1000/fps);
}

function Cross(x, y, size, corner, speed, funcCheck){
	this.x = x;
	this.y = y;
	this.animateX = x;
	this.animateY = y;
	this.size = size;
	this.corner = corner;
	this.speed = speed;
	this.funcCheck = funcCheck;
}

Cross.prototype.draw = function(){
	ctx.lineWidth = 5 * this.size;
	ctx.strokeStyle = 'white';
	ctx.translate(this.animateX, this.animateY);
	ctx.beginPath();
	ctx.rotate(this.speed * Math.PI / 180);
    this.speed += this.corner;
	ctx.moveTo(-(20*this.size)/2, -(20*this.size)/2);
	ctx.lineTo(20*this.size/2, 20*this.size/2);
	ctx.moveTo(-(20*this.size)/2, 20*this.size/2);
	ctx.lineTo(20*this.size/2, -(20*this.size)/2);
	ctx.stroke();
	ctx.setTransform(1, 0, 0, 1, 0, 0);
};

function createCross(){
	for (let i=0; i<=rand; i++){
		var x = Number(selfRandom(0, bodyWidth).toFixed());
		var y = Number(selfRandom(0, bodyHeight).toFixed());
		var funcCheck = Number(selfRandom(1, 2).toFixed());
		var size = Number(selfRandom(0.1, 0.6).toFixed(1));
		var corner = Number(selfRandom(0, 360).toFixed());
		var speed = Number(selfRandom(-0.2, 0.2).toFixed(1));
		arrCross.push(new Cross(x, y, size, corner, speed, funcCheck));
		arrCross[i].draw();
	}
}

function Circle(x, y, size, funcCheck){
	this.x = x;
	this.y = y;
	this.size = size;
	this.funcCheck = funcCheck;
	this.animateX = x;
	this.animateY = y;
}

Circle.prototype.draw = function(){
	ctx.lineWidth = 5 * this.size;
	ctx.strokeStyle = 'white';
	ctx.beginPath();
	ctx.arc(this.animateX, this.animateY, this.size * 12, 0, Math.PI * 2);
	ctx.stroke();
};

function createCircle(){
	for (let i=0; i<=rand; i++){
		var x = Number(selfRandom(0, bodyWidth).toFixed());
		var y = Number(selfRandom(0, bodyHeight).toFixed());
		var size = Number(selfRandom(0.1, 0.6).toFixed(1));
		var funcCheck = Number(selfRandom(1, 2).toFixed());
		arrCircle.push(new Circle(x, y, size, funcCheck));
		arrCircle[i].draw();
	}
}

function selfRandom(min, max){
  return Math.random() * (max - min) + min;
}

function nextPoint_1(x, y, time) {
  return {
    x: x + Math.sin((50 + x + (time / 10)) / 100) * 3,
    y: y + Math.sin((45 + x + (time / 10)) / 100) * 4
  };
}

function nextPoint_2(x, y, time){
  return {
    x: x + Math.sin((x + (time / 10)) / 100) * 5,
    y: y + Math.sin((10 + x + (time / 10)) / 100) * 2
  }
}

function animate(){
	ctx.clearRect(0, 0, bodyWidth, bodyHeight);
	for (let i=0; i<=arrCross.length-1; i++){
		var x_crcl = arrCircle[i].x;
		var y_crcl = arrCircle[i].y;
		var x_crs = arrCross[i].x;
		var y_crs = arrCross[i].y;
		var result_crcl = (arrCircle[i].funcCheck == 1) ? nextPoint_1(x_crcl, y_crcl, Date.now()) : nextPoint_2(x_crcl, y_crcl, Date.now());
		var result_crs = (arrCross[i].funcCheck == 1) ? nextPoint_1(x_crs, y_crs, Date.now()) : nextPoint_2(x_crs, y_crs, Date.now());
		arrCircle[i].animateX = result_crcl.x;
		arrCircle[i].animateY = result_crcl.y;
		arrCross[i].animateX = result_crs.x;
		arrCross[i].animateY = result_crs.y;
		arrCircle[i].draw();
		arrCross[i].draw();
	}
}