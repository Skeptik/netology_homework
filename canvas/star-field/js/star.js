addEventListener('load', inicialize, false);

const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
const styles = getComputedStyle(canvas);
const width = parseInt(styles.width, 10);
const height = parseInt(styles.height, 10);

function inicialize(){
	canvas.width = width;
	canvas.height = height;
	canvas.style.backgroundColor = 'black';
	canvas.addEventListener('click', onClick, false);
	randSky();
}

function onClick(){
	ctx.clearRect(0, 0, width, height);
	randSky();
}

function randSky(){
	var stars = Math.floor(selfRandom(200, 400));
	for (let i=0; i<=stars; i++){
		var sizeStar = Number(selfRandom(0, 1.1).toFixed(1));
		var wdth = Math.floor(selfRandom(0, width));
		var hght = Math.floor(selfRandom(0, height));
		ctx.globalAlpha = Number(selfRandom(0.8, 1).toFixed(1));
		ctx.fillStyle = randColor();
		ctx.beginPath();
		ctx.arc(wdth, hght, sizeStar, 0, Math.PI, false);
		ctx.fill();
		ctx.closePath();
	}
}

function randColor(){
	const arrColor = ['#ffffff','#ffe9c4','#d4fbff',];
	const result = Math.floor(selfRandom(0, arrColor.length));
	return arrColor[result];
}

function selfRandom(min, max){
  return Math.random() * (max - min) + min;
}
