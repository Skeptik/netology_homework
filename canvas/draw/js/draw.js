'use strict';

addEventListener('load', inicialization, false);

var canvas = document.querySelector('canvas');
var ctx = canvas.getContext("2d");
var arrPoints = [];
var indx1 = 0, indx2 = 1;
var width, height, chk;
var pressShift = false;
var currentHue = 0;
var painting = false;

var opts = {
  color: "hsl(hue, 100%, 50%)",
  radius: 100,
};



function inicialization(){
  addEventListener('resize', onResize, false);
  canvas.addEventListener('mousedown', onMouseDown, false);
  canvas.addEventListener('mousemove', onMousemove, false);
  canvas.addEventListener('mouseup', onMouseUp, false);
  canvas.addEventListener('mouseleave', onMouseLeave, false);
  document.addEventListener('keydown', keyDown, false);
  document.addEventListener('keyup', keyUp, false);
  document.addEventListener('dblclick', onDblclick, false);
  onResize();
}

function onDblclick(){
  ctx.clearRect(0, 0, width, height);
  arrPoints = [];
}

function onResize(){
  width = window.innerWidth;
  height = window.innerHeight;
  ctx.clearRect(0, 0, width, height);
  arrPoints = [];
  canvas.setAttribute('width', width);
  canvas.setAttribute('height', height);
}

function keyDown(e){
  if (e.key == 'Shift'){
    pressShift = true;
  }
}

function keyUp(){
  pressShift = false;
}

function onMouseLeave(){
  painting = false;
}

function onMouseDown(){
  painting = true;
};

function onMouseUp(){
  painting = false;
  arrPoints = [];
  indx1 = 0;
  indx2 = 1;
};

function checkShift(){
  (pressShift) ? currentHue-- : currentHue++;  
      if((currentHue == 360)){
        currentHue = 0;
      } else if (currentHue == 0) {
        currentHue = 359;
      }
}

function checkWidth(){
  if(opts.radius == 100){
    chk = true;
  }else if (opts.radius == 5){
    chk = false;
  }
  (chk) ? opts.radius-- : opts.radius++;
}

function onMousemove (e){
  if(painting){
    arrPoints.push([e.pageX, e.pageY]);
    if(arrPoints[indx1] && arrPoints[indx2]){
      checkShift();
      checkWidth();
      ctx.beginPath()
      ctx.lineWidth = 10*2;
      var currentColor = opts.color.replace("hue", currentHue);
      ctx.lineWidth = opts.radius;
      ctx.strokeStyle = currentColor;
      ctx.lineJoin = 'round';
      ctx.lineCap = 'round';
      ctx.moveTo(...arrPoints[indx1]);
      ctx.lineTo(...arrPoints[indx2]);
      ctx.stroke();
      ctx.closePath();
      indx1++;
      indx2++;
    }
  }
};