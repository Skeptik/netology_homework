addEventListener('load', initialize, false);

const arrA = document.getElementsByTagName('a');

function initialize() {	
  for (var i=0; i<=arrA.length-1; i++) {
  	arrA[i].addEventListener('click', click, false);
  }
}

function click (e){
  e.preventDefault();
  const hrefFullImage = e.target.parentNode;
  const fullImage = document.getElementById('view');
  for (var i=0; i<=arrA.length-1; i++){
  	if(arrA[i].classList[0] == 'gallery-current') {
  	  arrA[i].classList.remove('gallery-current');
  	}
  }
  fullImage.src = hrefFullImage.href;
  hrefFullImage.setAttribute('class', 'gallery-current');
}