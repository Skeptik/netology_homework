addEventListener('load', initialize, false);

const li = document.getElementsByTagName('li');
const audio = document.getElementsByTagName('audio');
const ul = document.getElementsByTagName('ul');
var code;
var flag = false;

const arrSoundLower = [
"sounds/lower/first.mp3",
"sounds/lower/second.mp3",
"sounds/lower/third.mp3",
"sounds/lower/fourth.mp3",
"sounds/lower/fifth.mp3"
];

const arrSoundMiddle = [
"sounds/middle/first.mp3",
"sounds/middle/second.mp3",
"sounds/middle/third.mp3",
"sounds/middle/fourth.mp3",
"sounds/middle/fifth.mp3"
];

const arrSoundHigher = [
"sounds/higher/first.mp3",
"sounds/higher/second.mp3",
"sounds/higher/third.mp3",
"sounds/higher/fourth.mp3",
"sounds/higher/fifth.mp3"
];

function initialize() {
  document.addEventListener('keydown', keyDown, false);
  document.addEventListener('keyup', keyUp, false);
  for (var i=0; i<=li.length-1; i++){
    li[i].addEventListener('click', play, false);
  }
  middle();
}

function play() {
  if (flag==true && code=='Shift') {
  	lower();
  }
  if (flag==true && code=='Control') {
  	higher();
  }
  if (!flag) {
  	middle();
  }
  const active = this.getElementsByTagName('audio');
  active[0].play();
}

function keyDown(e) {
  code = e.key;
  if (e.shiftKey) {
  	var classP1 = ul[0].classList.contains('higher') ? 'higher' : 'middle'; 
  	ul[0].classList.remove(classP1);
  	ul[0].classList.add('lower');
    flag = true;
  }
  if (e.ctrlKey) {
  	var classP2 = ul[0].classList.contains('middle') ? 'middle' : 'lower'; 
  	ul[0].classList.remove(classP2);
  	ul[0].classList.add('higher');
    flag = true;	
  }
}

function keyUp() {
  var classPianino = ul[0].classList.contains('higher') ? 'higher' : 'lower'; 
  ul[0].classList.remove(classPianino);
  ul[0].classList.add('middle');	
  flag = false;
}

function middle() {
  for (var i=0; i<=audio.length-1; i++) {
  	audio[i].src = arrSoundMiddle[i];
  }
}

function lower() {
  for (var i=0; i<=audio.length-1; i++) {
  	audio[i].src = arrSoundLower[i];
  }
}

function higher(){
  for (var i=0; i<=audio.length-1; i++) {
  	audio[i].src = arrSoundHigher[i];
  }
}