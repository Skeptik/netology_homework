var slides = [ 
	"i/airmax-jump.png", 
	"i/airmax-on-foot.png", 
	"i/airmax-playground.png", 
	"i/airmax-top-view.png", 
	"i/airmax.png"
	];
var currentSlide = 0;

addEventListener('load', main, false);

function main(){
	const slider = document.getElementById('slider');
	if (currentSlide > slides.length-1){
		currentSlide = 0;
	}
	slider.src = slides[currentSlide];
	currentSlide++;
	setTimeout(main, 5000);
}