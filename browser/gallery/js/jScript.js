addEventListener('load', main, false);

var currentGalery = -1;
var galery = [
"i/breuer-building.jpg",
"i/guggenheim-museum.jpg",
"i/headquarters.jpg",
"i/IAC.jpg",
"i/new-museum.jpg"
];

function main (){
	document.getElementById('prevPhoto').onclick = prevPhoto;
	document.getElementById('nextPhoto').onclick = nextPhoto;
}

function prevPhoto(){
	currentGalery--;
	if (currentGalery < 0){
		currentGalery = galery.length-1;
	}
	dispaly ();
}

function nextPhoto(){
	currentGalery++;
	if (currentGalery > galery.length-1){
		currentGalery = 0;
	}
	dispaly();
}

function dispaly (){
	const pPhoto = document.getElementById('currentPhoto');
	pPhoto.src = galery[currentGalery];
}