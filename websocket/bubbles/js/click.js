'use strinct';
const url = 'wss://neto-api.herokuapp.com/mouse';
const connection = new WebSocket(url);
window.addEventListener('beforeunload', ()=>{
	connection.close();
});
addEventListener('mousedown', onClick);
connection.addEventListener('open', open);
connection.addEventListener('error', error);

function onClick(e){
	coords = new Object();
	coords.x=e.pageX;
	coords.y=e.pageY;
	connection.send(JSON.stringify(coords));
}

function open(){
	showBubbles(connection);
}

function error(error){
	console.log('Ошибка ', error);
}
