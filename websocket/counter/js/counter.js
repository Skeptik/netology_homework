'use strict';

const counter = document.querySelector('.counter');
const errors = document.querySelector('.errors');
const url = 'wss://neto-api.herokuapp.com/counter';
const connection = new WebSocket(url);
connection.addEventListener('message', message);
connection.addEventListener('error', error);
window.addEventListener('beforeunload', ()=>{
	connection.close(1000, 'Работа закончена');
});

function message(event){
	var results = JSON.parse(event.data);
	counter.textContent = results['connections'];
	errors.textContent = results['errors'];
}

function error(error){
	console.log('Ошибка ', error);
}
