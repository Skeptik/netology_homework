'use strict'
const messagesContent = document.querySelector('.messages-content');
const button = document.querySelector('.message-submit');
const counter = document.querySelector('.counter');
const errors = document.querySelector('.errors');
const loadig = document.querySelector('.loading');
const input = document.querySelector('.message-input');
const url = 'wss://neto-api.herokuapp.com/chat';
const connection = new WebSocket(url);
connection.addEventListener('message', message);
connection.addEventListener('error', error);
connection.addEventListener('open', open);
connection.addEventListener('close', close);
button.addEventListener('click', onClick, false);
window.addEventListener('beforeunload', ()=>{
	connection.close();
});

function onClick(e){
	e.preventDefault();
	messagesContent.appendChild(tempPersonal(input.value));
	connection.send(input.value);
}

function open(){
	console.log('открыт');
	button.disabled=false;
	const chatStatus = document.querySelector('.chat-status');
	chatStatus.textContent = chatStatus.dataset.online;
	messagesContent.appendChild(tempStatus('Пользователь появился в сети'));
}

function message(event){
	console.log(event.data);
	if(event.data === '...'){
		messagesContent.appendChild(tempLoading());
		return;
	}else if (document.querySelector('.messages-content > .loading')){
		var test = document.querySelector('.messages-content > .loading');
		messagesContent.removeChild(test);
	}
	messagesContent.appendChild(tempInterlocutor(event.data));
}

function close(){
	button.disabled=true;
	const chatStatus = document.querySelector('.chat-status');
	chatStatus.textContent = chatStatus.dataset.offline;
	messagesContent.appendChild(tempStatus('Пользователь не в сети'));
}

function error(error){
	console.log('Ошибка ', error);
}

function tempLoading(){
	const tt =loadig.cloneNode(true); 
	return tt;
}

function tempStatus(text){
	const messageStatus = document.querySelector('.message-status');
	const msg = messageStatus.cloneNode(true);
	msg.firstElementChild.textContent=text;
	return msg;
}

function tempInterlocutor(text){
	const msg = loadig.nextElementSibling.cloneNode(true);
	msg.firstElementChild.nextElementSibling.textContent = text;
	msg.firstElementChild.nextElementSibling.nextElementSibling.textContent=time();
	return msg;
}

function tempPersonal(text){
	const msgPersonal = document.querySelector('.message-personal');
	const msg = msgPersonal.cloneNode(true);
	msg.firstElementChild.textContent = text;
	msg.firstElementChild.nextElementSibling.textContent=time();
	return msg;
}

function time(){
	var date = new Date();
	var hours = ('0' + date.getHours()).slice(-2);
	var minutes = ('0' + date.getMinutes()).slice(-2);
	return hours+':'+minutes;
}
