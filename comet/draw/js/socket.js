'use strict';
const ws = new WebSocket('wss://neto-api.herokuapp.com/draw');

ws.addEventListener('open', open);
window.addEventListener('beforeunload', ()=>{
	connection.close();
});

function open(){
	window.editor.addEventListener('update', update, false);
}

function update(e){
    e.canvas.toBlob((res)=>{
        ws.send(res);
    });
}