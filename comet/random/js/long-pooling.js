'use strict';
const urlLongPolin = 'https://neto-api.herokuapp.com/comet/long-pooling';
var clas, prevDiv, prevP, prevLP;

function request (){
    Promise.all([fetch(urlPolin), fetch(urlLongPolin)])
        .then(([res1, res2]) =>{
            res1.json()
                .then((data)=>{
                    LongAndPolin(data, 'polin');
                });
            res2.json()
                .then((data)=>{
                    LongAndPolin(data, 'long');
                });
        })
        .catch((error) => {
            console.log('Ошибка ' + error);
        });
}

function LongAndPolin(data, check){
    if (check == 'polin'){
        clas = 'pooling';
        prevDiv = prevP;
    }else{
        clas = 'long-pooling';
        prevDiv = prevLP;
    }

    const div = document.querySelectorAll('.'+ clas +' > div');
    (prevDiv) ? prevDiv.className = '' : '';
    for (let i=0; i<=div.length-1; i++){
        if (div[i].textContent == data){
                div[i].className = 'flip-it';
                (check == 'polin') ? prevP = div[i] : prevLP = div[i];
        }
    }
}

setInterval(request, 5000);