'use strict';
const urlWs = 'wss://neto-api.herokuapp.com/comet/websocket';
var prevWS;
const ws = new WebSocket(urlWs);

ws.addEventListener('message', message);
window.addEventListener('beforeunload', ()=>{
    connection.close();
});

function message(e){
    const div = document.querySelectorAll('.websocket > div');
    (prevWS) ? prevWS.className = '' : '';
    for (let i=0; i<=div.length-1; i++){
        if (div[i].textContent == e.data){
            div[i].className = 'flip-it';
            prevWS = div[i];
        }
    }
}