/* Данный JS код */

// Регулируем видимость карточки
function toggleCardVisible () {
 document.getElementById('content').classList.toggle('hidden');
 document.getElementById('card').classList.toggle('hidden');
}

const url = "https://neto-api.herokuapp.com/book/";
const xhr = new XMLHttpRequest();
xhr.addEventListener('load', onLoad);
xhr.open('GET', url);
xhr.send();

document.getElementById('close').addEventListener('click', toggleCardVisible);

document.getElementById('content').addEventListener('click', (event) => {
    let target = null;
    if (event.target.tagName === 'LI') {
        target = event.target;
    }
    if (event.target.parentNode.tagName === 'LI') {
        target = event.target.parentNode;
    }

    if (target) {
      toggleCardVisible();
      document.getElementById('card-title').innerHTML = target.dataset.title;
      document.getElementById('card-author').innerHTML = target.dataset.author;
      document.getElementById('card-info').innerHTML = target.dataset.info;
      document.getElementById('card-price').innerHTML = target.dataset.price;
    }
});

function onLoad(){
  document.querySelector('#content').innerHTML = '';
  if(xhr.status === 200){
    var results = JSON.parse(xhr.responseText);
  }else{
    return;
  }
  for (let i =0; i<=results.length-1; i++){
    console.log(results[i]);
    var objLi = document.createElement('li');
    var objImg = document.createElement('img');
    objLi.setAttribute('data-title', results[i].title);
    objLi.setAttribute('data-author', results[i].author.name);
    objLi.setAttribute('data-info', results[i].info);
    objLi.setAttribute('data-price', results[i].price);
    objImg.setAttribute('src', results[i].cover.small);
    objLi.appendChild(objImg);
    document.querySelector('#content').appendChild(objLi);
  }
}
