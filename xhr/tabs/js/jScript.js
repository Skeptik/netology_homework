addEventListener('load', inicialize, false);
const tagA = document.querySelectorAll('a');
const divContent = document.querySelector('#content');
const divPreloader = document.querySelector('#preloader');
const xhr = new XMLHttpRequest();
var active;

function inicialize (){
	for (let i=0; i<=tagA.length-1; i++){
		tagA[i].addEventListener('click', activeTab, false);
	}
	const event = new Event("click");
    tagA[0].dispatchEvent(event);
}

function activeTab (e){
	e.preventDefault();
	target = e.target.getAttribute('href');
	xhr.addEventListener('load', onLoad);
	xhr.addEventListener('loadstart', onLoadStartEnd);
	xhr.addEventListener('loadend', onLoadStartEnd);
	xhr.open('GET', "http://localhost/skep/" + target);
	xhr.send();
	active = e.target;
}

function onLoad(){
	activeClass();
	divContent.innerHTML = xhr.responseText;
}

function activeClass (){
	if (active.classList[0] === 'active'){
		return;
	}
	tagA[0].classList.toggle('active');
	tagA[1].classList.toggle('active');
}

function onLoadStartEnd(){
	divPreloader.classList.toggle('hidden');
}