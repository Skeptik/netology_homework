addEventListener('load', main, false);
var summValue = 0;
var summCheck = 0;

function getPriceFormatted(value) {
  return  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

function main(){
	const divContainer = document.querySelector('#container');
	divContainer.addEventListener('click', click, false);
}

function click(e){
	if(!e.target.classList.contains('add')){
		return false;
	}
	var price = e.target.getAttribute('data-price');
	var spanQuantity = document.getElementById('cart-count');
	var spanValue = document.getElementById('cart-total-price');
	summValue += Number(price);
	summCheck++;
	spanValue.innerHTML = getPriceFormatted(summValue);
	spanQuantity.innerHTML = summCheck;
}