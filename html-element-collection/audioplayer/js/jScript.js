addEventListener('load', initialize, false);

var counterSound = -1;
const aux = document.getElementsByTagName('audio');
const arrSounds = [
'This is it band',
'LA Fusion Jam',
'LA Chill Tour'
]; 


function initialize(){
	const arr = document.getElementsByTagName('button');	
	for (var item = 0; item < arr.length; item++){
		arr[item].onclick = realization;
	}
}

function realization(){
	var iPause = document.getElementsByClassName('fa-pause');
	var pauseDisplay = window.getComputedStyle(iPause[0]).display;
	switch (this.className) {
		case 'playstate':
			if (pauseDisplay == 'none')
			{
				play();
				aux[0].play();
			}else{
				paused();
				aux[0].pause();
			}		
		break;
		case 'stop':
			if (pauseDisplay == 'block'){
				paused();
			}
			aux[0].pause();
			aux[0].currentTime = 0.0;
		break;
		case 'next':
			counterSound++;
			if (counterSound > arrSounds.length-1){
				counterSound = 0;
			}
			if (pauseDisplay == 'none'){
				play();
			}
			game (counterSound);
		break;
		case 'back':
			counterSound--;
			if (counterSound < 0){
				counterSound = arrSounds.length-1;
			}
			if (pauseDisplay == 'none'){
					play();
			}
			game(counterSound);
		break;
	}
}

function play(){
	document.getElementsByClassName('fa-play')[0].style.display = 'none';
	document.getElementsByClassName('fa-pause')[0].style.display = 'block';
	document.getElementsByClassName('mediaplayer')[0].classList.add('play');
}

function paused(){
	document.getElementsByClassName('fa-play')[0].style.display = 'block';
	document.getElementsByClassName('fa-pause')[0].style.display = 'none';
	document.getElementsByClassName('mediaplayer')[0].classList.remove('play');
}

function game (counterSound){
	document.getElementsByClassName('title')[0].title = arrSounds[counterSound];
	aux[0].src = 'mp3/' + arrSounds[counterSound] + '.mp3';
	aux[0].play();
}

aux[0].ontimeupdate = function (){
	if (aux[0].currentTime >= aux[0].duration){
		aux[0].currentTime = 0;
		paused();
	}
}