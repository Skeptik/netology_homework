addEventListener('load', inicialize, false);

function inicialize(){
	const arr = document.getElementsByClassName('drum-kit__drum');
	for (var item = 0; item < arr.length; item++){
		arr[item].onclick = click;
	}
}

function click (){
	const check = this.getElementsByTagName('audio'); 
	check[0].play();
}
