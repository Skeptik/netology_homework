'use strict';

if (navigator.mediaDevices === undefined) {
  navigator.mediaDevices = {};
}

if (navigator.mediaDevices.getUserMedia === undefined) {
  navigator.mediaDevices.getUserMedia = function (constraints) {
    var getUserMedia = navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia;

    if (!getUserMedia) {
      return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
    }
    return new Promise((resolve, reject) => {
      getUserMedia.call(navigator, constraints, resolve, reject);
    });
  }
}
var prom;
function createThumbnail(video) {
  return new Promise((done, fail) => {
    const preview = document.createElement('video');
    preview.src = URL.createObjectURL(video);
    preview.addEventListener('loadeddata', () => preview.currentTime = 2);
    preview.addEventListener('seeked', () => {
      const snapshot = document.createElement('canvas');
      const context = snapshot.getContext('2d');
      snapshot.width = preview.videoWidth;
      snapshot.height = preview.videoHeight;
      context.drawImage(preview, 0, 0);
      snapshot.toBlob(done);
    });
  });
}
var recoder;
let chunks = [];
function record(app) {
  return new Promise((done, fail) => {
    app.mode = 'preparing';
    navigator.mediaDevices
      .getUserMedia(app.config)
      .then(stream=>{
        setTimeout(()=>{
          app.mode = 'recording';
          app.preview.srcObject = stream;
          recoder = new MediaRecorder(stream);
          recoder.addEventListener('dataavailable', (e)=>{
            chunks.push(e.data);
          });
          recoder.addEventListener('stop', (e)=>{
            const rec = new Blob(chunks, {'type': recoder.mimeType});
            chunks = [];
            recoder = stream = null;
            function obj (video, img){
              this.video = video;
              this.frame = img;
            }
            createThumbnail(rec)
              .then((res)=>{
                done(new obj(rec, res));
              })
              .catch(err=>console.warn('Ошибка '+ err));
          });
            recoder.start();
        },1000);

        setTimeout(() => {
          stream.getTracks().forEach(track => track.stop());
          app.mode = 'sending';
          recoder.stop();
        }, app.limit+1000);
      })
      .catch(err=>console.warn('Ошибка '+ err));
  });
}