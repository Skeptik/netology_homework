'use strict';
const errorMesage = document.querySelector('#error-message');
const button = document.querySelector('button');
const controls = document.querySelector('.controls');
const app = document.querySelector('.app');
const list = document.querySelector('.list');
const url = 'https://neto-api.herokuapp.com/photo-booth';
var video;
var arrImage = [];

addEventListener('load', inicialization, false);

function inicialization (){
    if(navigator.mediaDevices){
        button.addEventListener('click', clickPhoto, false);
        list.addEventListener('click', clickList, false);
        const audioObj = document.createElement('audio');
        audioObj.src = 'audio/click.mp3';
        button.appendChild(audioObj);
        const videoObj = document.createElement('video');
        videoObj.autoplay = true;
        app.insertBefore(videoObj, app.children[0]);
        video = document.querySelector('video');
        controls.classList.toggle('visible');
        navigator.mediaDevices
        .getUserMedia({video:true, audio:false})
        .then(stream=>{
            video.src = URL.createObjectURL(stream);
        })
        .catch((err)=>{
            errorMesage.style.display = 'block';
            errorMesage.textContent = err;
        });
    }else{
        errorMesage.style.display = 'block';
        errorMesage.textContent = 'Ошибка, navigator.mediaDevices';
    }
}

function clickList(e){
    if (e.target.textContent == 'delete'){
        const el = e.target.parentElement.parentElement.firstElementChild;
        const elem = e.target.parentElement.parentElement.parentElement;
        list.removeChild(elem);
        delete arrImage[el.download];
    }
    if(e.target.textContent == 'file_upload'){
        const el = e.target.parentElement.parentElement.firstElementChild;
        const im = document.querySelector('.list > figure > img#im'+el.download);
        const canvas = document.createElement('canvas');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        console.log(video.videoWidth, video.videoHeight);
        var ct = canvas.getContext("2d");
        ct.drawImage(im, 0, 0);
        return new Promise((res, rej)=>{
            canvas.toBlob(res);
        })
        .then(dat=>{
            console.log(dat);
            var formData = new FormData();
            formData.append('image', dat);
            var promise = fetch(url,{
            body : formData,
            credentials: 'omit',
            method: 'POST',
            })
            .then((res)=>{
                if(200<=res.status && res.status<300){
                    return res;
                }
                throw new Error(response.statusText);
            })
            .then((res) => res.json())
            .then((data) =>{
                console.log(data);
            })
            .catch((error)=>{
                console.log('Ошибка ' + error);
            });
        });
    }
}

function clickPhoto(e){
    list.textContent = '';
    const aux = document.querySelector('audio');
    aux.pause();
    aux.play();
    const canvas = document.createElement('canvas');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(video, 0, 0);
    arrImage.push(canvas.toDataURL());
    for(let i=0; i<=arrImage.length-1; i++){
        if(arrImage[i]){
            const figureObj = document.createElement('figure');
            const imgObj = document.createElement('img');
            imgObj.src = arrImage[i];
            imgObj.id = 'im'+i;
            const figcaptionObj = document.createElement('figcaption');
            const aObj_1 = document.createElement('a');
            aObj_1.href = arrImage[i];
            aObj_1.setAttribute('download', i);
            const iObj_1 = document.createElement('i');
            iObj_1.className = 'material-icons';
            iObj_1.textContent = 'file_download';
            aObj_1.appendChild(iObj_1);
            const aObj_2 = document.createElement('a');
            const iObj_2 = document.createElement('i');
            iObj_2.className = 'material-icons';
            iObj_2.textContent = 'file_upload';
            aObj_2.appendChild(iObj_2);
            const aObj_3 = document.createElement('a');
            const iObj_3 = document.createElement('i');
            iObj_3.className = 'material-icons';
            iObj_3.textContent = 'delete';
            aObj_3.appendChild(iObj_3);
            figcaptionObj.appendChild(aObj_1);
            figcaptionObj.appendChild(aObj_2);
            figcaptionObj.appendChild(aObj_3);
            figureObj.appendChild(imgObj);
            figureObj.appendChild(figcaptionObj);
            if (list.firstChild){
                list.insertBefore(figureObj, list.children[0]);
            }else{
                list.appendChild(figureObj);
            }
        }
    }
}