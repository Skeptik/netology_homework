'use strict';
addEventListener('load', inicialization, false);

const eye = document.querySelector('.big-book__pupil');
var width = document.body.clientWidth;
var height = document.body.clientHeight;
var coords;
var chk;
function inicialization(){
    document.body.addEventListener('mousemove', mouse, false);
}

function mouse(e){
    coords = eye.getBoundingClientRect();
    eye.style.setProperty('--pupil-x', check(e.pageX, width)+'px');
    eye.style.setProperty('--pupil-y', check(e.pageY, height)+'px');
    if(distanse(e.pageX, e.pageY)<100){
        chk = 3;
    }else if(distanse(e.pageX, e.pageY)>500){
        chk = 1;
    }else{
        chk = 2;
    }
    eye.style.setProperty('--pupil-size', chk);
}

function check(coord, size){
    return (100*coord/size)*60/100-30;
}

function distanse(x, y){
    return Math.sqrt(Math.pow(x-coords.x, 2)+Math.pow(y-coords.y, 2));
}
